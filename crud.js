let http = require("http")

// mock/dummy data
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

http.createServer(function(req, res) {

	// route for returning all items upon receiving GET Method
	if(req.url == "/users" && req.method == "GET") {
		res.writeHead(200, {"Content-Type": "application/json"})
		// input has to be data type STRING for the application to read the incoming data properly. 
		res.write(JSON.stringify(directory))
		// ends the response process
		res.end()
	}

	if(req.url == "/users" && req.method == "POST") {
		// declare and initialize reqBody variable to an empty string
		// this will act as a placeholder for the data to be created later 
		let reqBody = ""

		// this is where data insertion happens to our mock/dummy data
		req.on("data", function(data) {
			// assigns the data retrieved from the stream to reqBody
			reqBody += data;
		})

		// response end step -> only runs after the request has completely been set
		req.on("end", function() {
			console.log(typeof reqBody)

			// Converts the string to JSON
			reqBody = JSON.parse(reqBody)

			// Create a new object representing the new mock data record
			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}

			directory.push(newUser)

			console.log(directory)

			res.writeHead(200, {"Content-Type": "application/json"})
			res.write(JSON.stringify(newUser))
			res.end()
		})
	}
}).listen(3000)

console.log("CRUD is not running at port 3000")